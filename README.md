# Vacuum Gauge Controller MKS 937B

EPICS module to provide communications and read/write data from/to MKS 937B vacuum gauge controller

## Startup Example

`iocsh -r vac_ctrl_mks937b,catane -c 'requireSnippet(source-gnd.cmd, "DEVNAME=VEG-10010,CONNAME=mks937b,ALOTA=gauge-mks-vgp,SLOTB=gauge-mks-vgc,SLOTC=gauge-mks-vgp")'`
