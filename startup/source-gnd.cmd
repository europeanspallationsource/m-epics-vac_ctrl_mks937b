# @field DEVNAME
# @type STRING
# MKS937B's device name.

# @field CONNAME
# @type STRING
# Connection name to the MKS937B's device.

# @field ADDRESS
# @type STRING
# Address parameter on the MKS937B's device.

# @field SLOTA
# @type STRING
# MKS937B's A slot device module name.

# @field SLOTB
# @type STRING
# MKS937B's B slot device module name.

# @field SLOTC
# @type STRING
# MKS937B's C slot device module name.

require vac_gauge_mks_vgp, 2.0.0-catania
require vac_gauge_mks_vgc, 2.0.0-catania

drvAsynIPPortConfigure("mks937b", "10.10.1.21:4001")

dbLoadRecords("vac-$(SLOTA).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=1, PORT2=2")
dbLoadRecords("vac-$(SLOTB).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=3, PORT2=4")
dbLoadRecords("vac-$(SLOTC).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=5, PORT2=6")
dbLoadRecords("vac-ctrl-mks937b.db", "DEVNAME = $(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS)")
